Fecha: 10/05/2024
Usuario: William Suarez

Descripcion: Prueba de DSS
- Pagina Inicial: General.html
    Muestra tabla de datos obtenidos en data/datos.json
    Boton de agregar datos a la tabla (deriva a ingreso.html)

- Pagina secundaria: ingreso.html
    Solicitud de Nombre, Apellido, Edad, Telefono, los cuales seran sanitizados y almacenados en el JSON.
    Deriva a pagina principal pero como "/inicio" GET, para evitar duplicacion de ingresos al recargar página.

Atentamente.