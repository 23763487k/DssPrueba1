const express = require('express')

// Importamos solo lo que necesitamos, Router
const router = express.Router()

// Indicamos de donde saca los controladores
const controller = require('../controllers/infoController')

router.get('/', controller.inicio)
router.get('/inicio', controller.inicio)
router.get('/ingreso', controller.ingreso)
router.post('/general', controller.sanitizado)

module.exports = router