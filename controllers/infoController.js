const fs = require('fs').promises
const path = require('path')

const { limpiarCodigoSanitizer } = require('../utils/functions')
const { leerDatos } = require('../utils/functions')
const { guardarDatos } = require('../utils/functions')

// Configurar el HTML inicial de la carpeta views
const inicio = async(req, res) => {
  datos = await leerDatos() // Leyendo DATOS del JSON
  res.render('../views/general.html', {locals:{ data: datos }}) // Pasando la variable 'datos' a la plantilla
}

// Configurar el HTML de la página de ingreso de Datos
const ingreso = async(req, res) => {
  res.render('../views/ingreso.html')
}

// Limpiar scripts y guardar datos
const sanitizado = async (req, res) => {
  const { nombre, apellido, edad, telefono } = await limpiarCodigoSanitizer(req.body)
  let datos = []

  try {
    datos = await leerDatos()                         // Leyendo DATOS del JSON
    datos.push({ nombre, apellido, edad, telefono })  // Insertando NUEVOS datos en el JSON
    await guardarDatos(datos)                         // Guardando los datos en el JSON
    res.redirect('/inicio')                           // Redirigiendo a la página de inicio para evitar duplicacion de ingresos
  } catch (error) {
    console.error('Error al guardar los datos:', error)
    res.status(500).send('Internal server error: error al guardar los datos') // Error al guardar
  }
}

module.exports = {
  inicio,
  ingreso,
  sanitizado,
}
  