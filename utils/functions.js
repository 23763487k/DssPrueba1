var sanitizer = require('sanitizer')
var fs = require('fs').promises
const path = require('path')

// Funcion para Sanitizer en el login
const limpiarCodigoSanitizer = async(datos) =>{
  var {nombre, apellido, edad, telefono} = datos 
  nombre = sanitizer.escape(nombre)
  apellido = sanitizer.escape(apellido)
  edad = sanitizer.escape(edad)
  telefono = sanitizer.escape(telefono)
  const normalized = {nombre, apellido, edad, telefono}
  return normalized
}

// Funcions para leer datos del JSON
const leerDatos = async () => {
  const filePath = path.join(__dirname, '../data/datos.json')
  try {
    const datosAlmacenados = await fs.readFile(filePath, 'utf-8')
    if (datosAlmacenados) {
      return JSON.parse(datosAlmacenados)
    }
  } catch (err) {
    if (err.code !== 'ENOENT') {
      console.error('Error al leer los datos almacenados:', err)
    }
  }
  return []
}

// Funcion para guardar los datos en JSON
const guardarDatos = async (datos) => {
  const filePath = path.join(__dirname, '../data/datos.json');
  await fs.writeFile(filePath, JSON.stringify(datos, null, 2), 'utf-8')
}

// Exportando módulos de las funciones creadas
module.exports = {
  limpiarCodigoSanitizer,
  leerDatos,
  guardarDatos
}