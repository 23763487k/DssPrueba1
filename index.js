const express = require('express')
const app = express()
const es6Renderer = require('express-es6-template-engine')

// indicamos que hay objetos JSON
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

//Indicamos donde estan las rutas
app.use(require('./routes/infoRoutes'))

// Indicamos para que trabaje conjunto con HTML
app.engine('html', es6Renderer)

// Iniciando Servidor
app.listen(3000, () => {
  console.log('Servidor en espera de solicitudes... Localhost:3000')
})